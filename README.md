# SwiftUIRTF
The purpose of this project is to build a Rich Text File editor with styling views and stored user styles. The hope is to make this base of an app I am building for note-taking and to share with others. I am planning to put the basic editor on the AppStore for free once the most significant features are complete.

The general concept for the whole app looks at easy ways to interact with NSAttributedStrings and their attributes. More Details to come.