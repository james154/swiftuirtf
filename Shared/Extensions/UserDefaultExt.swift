//
//  UserDefaultExt.swift
//  SwiftUITest
//
//  Created by James Sumners on 6/28/20.
//  Copyright © 2020 James Sumners. All rights reserved.
//

import UIKit

extension UserDefaults {
func set<Element: Codable>(value: Element, forKey key: String) {
        let data = try? JSONEncoder().encode(value)
        UserDefaults.standard.setValue(data, forKey: key)
    }
func codable<Element: Codable>(forKey key: String) -> Element? {
        guard let data = UserDefaults.standard.data(forKey: key) else { return nil }
        let element = try? JSONDecoder().decode(Element.self, from: data)
        return element
    }
}
