//
//  TextViewExt.swift
//  SwiftUIRTF
//
//  Created by James Sumners on 9/1/20.
//

import SwiftUI

extension UITextView {
    var selectionAttributes: RichTextAttributes {
        get {
//            print("Selected Range: \(self.selectedRange)")
            if self.selectedRange.length == 0 {
                let rtAttr = RichTextAttributes(self.typingAttributes)
                return rtAttr
            }
            else {
                let rtAttr = RichTextAttributes(self.attributedText, range: self.selectedRange)
//                print("------Selected Font Bold------\n\(rtAttr.bold)")
                return rtAttr
            }
        }
//        set {
//
//            print("----OldValue UITextView----\nbold: \(self.selectionAttributes.bold)")
//            print("----NewValue UITextView----\nbold: \(newValue.bold)")
//            if self.selectedRange.length == 0 {
//                self.typingAttributes = newValue.attributes
//            }
//            else {
//                let previousSelectedRange = self.selectedRange
//                let mutedString = NSMutableAttributedString(attributedString: self.attributedText)
//                mutedString.addAttributes(newValue.attributes, range: self.selectedRange)
//
//                self.attributedText = mutedString
//                self.typingAttributes = newValue.attributes
//                self.selectedRange = previousSelectedRange
//            }
//
//            print("----After UITextView----\nbold: \(self.selectionAttributes.bold)")
//        }
    }
    
    #if ALT_DID_CHANGE_VIEW
    
    #else
    
    internal func processTextViewChanges()
    {
        //
    }
    
    #endif
    
    #if ALT_DID_CHANGE_VIEW
    
    #else
    
    internal func processSelectionChanges()
    {
        //
    }
    
    #endif
    
    #if ALT_REPLACE_TEXT
    
    #else
    internal func processReplacement(range: NSRange, text: String) -> Bool
    {
//        print("Current \(selectedRange) - New \(range)")

        
        
        return false
    }
    #endif
    
    #if ALT_BEGIN_EDIT
    
    #else
    internal func processBeginEdit()
    {
        //
    }
    #endif
    
    #if ALT_END_EDIT
    
    #else
    internal func processEndEdit()
    {
        //
    }
    #endif
    
    internal func setInputAccessory()
    {
        let bar = UIToolbar()
        
        let textformat = UIBarButtonItem(image: UIImage(systemName: "textformat"), style: .plain, target: self, action: #selector(self.editView(sender:)))
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneButtonAction))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        bar.items = [textformat, spacer, done]
        bar.sizeToFit()
        self.inputAccessoryView = bar
    }
    
    @objc internal func editView(sender: Selector)
    {
        self.editView()
        
    }
    
    func editView() {
        let formatView = ParagraphStyleView(uiTextView: self)
            .transition(
                .move(edge: .bottom))
            .animation(.default)
        let formatViewController = UIHostingController(rootView: formatView)
        formatViewController.view.frame = CGRect(x: 0, y: self.frame.size.height-225, width: self.frame.size.width, height: 350)

        formatViewController.view.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        formatViewController.view.tag = 1984
        
        self.resignFirstResponder()
        self.inputAccessoryView = nil
        
        if self.inputView?.tag == 1984 {
            self.inputView = nil
        }
        
        if self.inputView != formatViewController.view {
            self.inputView = formatViewController.view
        }
        self.becomeFirstResponder()
    }
    
    func exitInputView() {
        self.resignFirstResponder()
        self.inputView = nil
        if self.inputAccessoryView == nil
        {
            self.setInputAccessory()
        }
//        self.keyboardType = .default
        self.becomeFirstResponder()
    }
    
    @objc internal func doneButtonAction(sender: Selector){
        self.endEditing(true)
    }
    
    internal func updateSelectedAttributes(richAttr: RichTextAttributes) {
        
        if self.selectedRange.length == 0 {
            self.typingAttributes = richAttr.attributes
        }
        else {
            let previousSelectedRange = self.selectedRange
            let mutedString = NSMutableAttributedString(attributedString: self.attributedText)
            mutedString.addAttributes(richAttr.attributes, range: self.selectedRange)
            
            self.attributedText = mutedString
            self.typingAttributes = richAttr.attributes
            self.selectedRange = previousSelectedRange
            
            self.scrollRangeToVisible(self.selectedRange)
        }
        
    }
    
}
