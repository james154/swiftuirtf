//
//  FontExt.swift
//  SwiftUITest
//
//  Created by James Sumners on 6/25/20.
//  Copyright © 2020 James Sumners. All rights reserved.
//

import SwiftUI

extension UIFont {
    
    var font: Font {
        get {
            return Font(self)
        }
    }
    
    var fontIndex: Int {
        get {
            for index in 0..<sortedFamilies.count {
                if self.sortedFamilies[index] == self.familyName {
                    return index
                }
            }
            return 0
        }
    }
    
    func withTraits(_ traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        
        // create a new font descriptor with the given traits
        if let fd = fontDescriptor.withSymbolicTraits(traits) {
            // return a new font with the created font descriptor
            return UIFont(descriptor: fd, size: pointSize)
        }
        
        // the given traits couldn't be applied, return self
        return self
    }
    
    func italicToggle() -> UIFont {
        if self.fontDescriptor.symbolicTraits.contains(.traitItalic) {
            var traits = self.fontDescriptor.symbolicTraits
            traits.remove(.traitItalic)
            return withTraits(traits)
        }
        else {
            var traits = self.fontDescriptor.symbolicTraits
            
            traits.insert(.traitItalic)
            
            return withTraits(traits)
        }
    }
    
    func boldToggle() -> UIFont {
        if self.fontDescriptor.symbolicTraits.contains(.traitBold) {
            var traits = self.fontDescriptor.symbolicTraits
            traits.remove(.traitBold)
            return withTraits(traits)
        }
        else {
            var traits = self.fontDescriptor.symbolicTraits
            
            traits.insert(.traitBold)
            
            return withTraits(traits)
        }
    }
    
    var isBold: Bool {
        get {
            let traits = self.fontDescriptor.symbolicTraits
            
            if traits.contains(.traitBold) {
                return true
            }
            
            return false
        }
    }
    
    var isItalic: Bool {
        get {
            let traits = self.fontDescriptor.symbolicTraits
            
            if traits.contains(.traitItalic) {
                return true
            }
            
            return false
        }
    }
    
    var sortedFamilies: [String] {
        get {
            return UIFont.familyNames.sorted()
        }
    }
}
