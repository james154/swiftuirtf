//
//  RichTextAttributes.swift
//  SwiftUIRTF
//
//  Purpose:
//  This class has one main attribute the attributes
//  which is an [NSAttributedString.Key:Any] dictionary.
//  With in this class different types of attributes
//  can be added, removed and updated.
//
//  Created by James Sumners on 9/10/20.
//

import SwiftUI

class RichTextAttributes: ObservableObject, Equatable {
    // Equatable function for comparing two
    // RichTextAttributes.
    static func == (lhs: RichTextAttributes, rhs: RichTextAttributes) -> Bool {
        NSDictionary(dictionary: lhs.attributes).isEqual(to: rhs.attributes)
    }
    
    // May be used in the future to handle different attributes.
    enum AttributeKeys: String {
        
        case font = "font"
        case paragraphStyle = "paragraphStyle"
        case foregroundColor = "foregroundColor"
        case backgroundColor = "backgroundColor"
        case ligature = "ligature"
        case kern = "kern"
        case tracking = "tracking"
        case strikethroughStyle = "strikethroughStyle"
        case underlineStyle = "underlineStyle"
        case strokeColor = "strokeColor"
        case strokeWidth = "strokeWidth"
        case shadow = "shadow"
        case textEffect = "textEffect"
        case attachment = "attachment"
        case link = "link"
        case baselineOffset = "baselineOffset"
        case underlineColor = "underlineColor"
        case strikethroughColor = "strikethroughColor"
        case obliqueness = "obliqueness"
        case expansion = "expansion"
        case writingDirection = "writingDirection"
        case verticalGlyphForm = "verticalGlyphForm"
    }
    
    // this is just for testing the behavior of the enumeration
    let test = NSAttributedString.Key.font.rawValue
    
    // Variable to store all NSAttributedString attributes.
    @Published var attributes: [NSAttributedString.Key : Any]
    
    var allowUpdate = false
    
    // Variable used to reference the font index within
    // the FamilyNames.sorted array.
    @Published var fontIndex: Int = 0 {
        // When this variable is set update the font attributed
        didSet {
            let tempFont = self.font(fromIndex: fontIndex, size: CGFloat(pointSize))
            
            let oldFamilyName = familyNames[oldValue]
            
            // Is old family font name different then the new one?
            if oldFamilyName != tempFont.familyName {
                attributes.updateValue(tempFont, forKey: .font)
            }
            
        }
    }
    
    func font(fromIndex: Int, size: CGFloat?) -> UIFont {
        var tempIndex: Int = fromIndex
        
        if fromIndex > self.familyNames.count ||
            fromIndex < 0{
            tempIndex = 0
        }
        
        if size != nil {
            return UIFont(name: self.familyNames[tempIndex], size: size!)!
        }
        else {
            return UIFont(name: familyNames[tempIndex], size: CGFloat(pointSize))!
        }
    }
    
    public let familyNames = UIFont.familyNames.sorted()
    
    @Published var foregroundColor: UIColor = UIColor.black {
//        get {
//            if let uiColor = attributes[.foregroundColor] as? UIColor {
//                return uiColor
//            }
//            else {
//                return UIColor.black
//            }
//        }
        didSet {
            attributes.updateValue(foregroundColor, forKey: .foregroundColor)
        }
    }
    
    var foregroundCGColor: CGColor {
        get {
            return foregroundColor.cgColor
        }
        set {
            foregroundColor = UIColor(cgColor: newValue)
        }
    }
    
    @Published var bold: Bool = false {
//        get {
//            var isBold = false
//
//            if let font = attributes[.font] as? UIFont {
////                isBold = font.isBold
//                isBold = font.fontDescriptor.symbolicTraits.contains(.traitBold)
//            }
//            print("RichText isBold \(isBold)")
//            return isBold
//        }
        didSet {
            if let font = attributes[.font] as? UIFont {
                if bold != font.isBold {
                    let tempFont = font.boldToggle()
                    attributes.updateValue(tempFont, forKey: .font)
                    if bold != tempFont.isBold {
                        bold = oldValue
                    }
                }
                
            }
        }
    }
    
    @Published var italic: Bool = false {
//        get {
//            var isItalic = false
//
//            if let font = attributes[.font] as? UIFont {
//                isItalic = font.isItalic
//            }
//
//            return isItalic
//        }
        didSet {
            if let font = attributes[.font] as? UIFont {
                if font.isItalic != italic {
                    let tempFont = font.italicToggle()
                    attributes.updateValue(tempFont, forKey: .font)
                    if italic != tempFont.isItalic {
                        italic = oldValue
                    }
                }
            }
        }
    }
    
    @Published var underline: Bool = false {
//        get {
//            var isUnderline = false
//
//            if let attrUnderline = attributes[.underlineStyle] as? Int {
//                if attrUnderline == NSUnderlineStyle.single.rawValue {
//                    isUnderline = true
//                }
//            }
//            return isUnderline
//        }
        didSet {
            if underline {
                attributes.updateValue(NSUnderlineStyle.single.rawValue, forKey: .underlineStyle)
            }
            else {
                attributes.updateValue(0, forKey: .underlineStyle)
            }
        }
    }
    
    @Published var strikethrough: Bool = false {
//        get {
//            var isStrikethrough = false
//
//            if let attrUnderline = attributes[.strikethroughStyle] as? Int {
//                if attrUnderline == NSUnderlineStyle.single.rawValue {
//                    isStrikethrough = true
//                }
//            }
//            return isStrikethrough
//        }
        didSet {
            if strikethrough {
                attributes.updateValue(NSUnderlineStyle.single.rawValue, forKey: .strikethroughStyle)
            }
            else {
                attributes.updateValue(0, forKey: .strikethroughStyle)
            }
        }
    }
    
    @Published var fontSelection: FontSelection = FontSelection(index: 0, styleName: "") {
//        get {
//            return FontSelection(self)
//        }
        didSet {
            let font = fontSelection.font
            self.attributes.updateValue(font, forKey: .font)
            self.underline = fontSelection.underline
            self.strikethrough = fontSelection.strikethrough
        }
    }
    
    @Published var uiFont: UIFont = UIFont.systemFont(ofSize: 16) {
//        get {
//            if let font = attributes[.font] as? UIFont {
//                return font
//            }
//            else {
//                return UIFont()
//            }
//        }
        didSet {
            attributes.updateValue(uiFont, forKey: .font)
        }
    }
    
    @Published var pointSize: Int = 0 {
//        get {
//            if let font = attributes[.font] as? UIFont {
//                return Int(font.pointSize)
//            }
//            else {
//                return 16
//            }
//        }
        didSet {
            if let font = attributes[.font] as? UIFont {
                let tempFont = font.withSize(CGFloat(pointSize))
                
                attributes.updateValue(tempFont, forKey: .font)
            }
        }
    }
    
    @Published var selectedRange: NSRange
    
    var name: String = "_"
    
    init(_ with: [NSAttributedString.Key : Any]) {
        attributes = with
        selectedRange = NSMakeRange(0, 0)
        
        for attribute in attributes {
            if (attribute.key == NSAttributedString.Key.font)
            {
                if let tempFont = attribute.value as? UIFont {
                    uiFont = tempFont
                    fontIndex = tempFont.fontIndex
                    fontSelection = FontSelection(index: tempFont.fontIndex, styleName: "")
                    pointSize = Int(tempFont.pointSize)
                    print("fontName: \(tempFont.fontName)")

                    if tempFont.fontDescriptor.symbolicTraits.contains(.traitBold) {
                        print("*******INIT BOLD******")
                        bold = true
                    }
                    if tempFont.fontDescriptor.symbolicTraits.contains(.traitItalic) {
                        print("*******INIT ITALIC******")
                        italic = true
                    }
                    
                }
            }
            else if (attribute.key == NSAttributedString.Key.underlineStyle)
            {
                if let underlineValue = attribute.value as? Int {
                    if underlineValue == NSUnderlineStyle.single.rawValue {
                        print("*******INIT UNDERLINE******")
                        underline = true
                    }
                }
            }
            else if (attribute.key == NSAttributedString.Key.strikethroughStyle)
            {
                if let strikethrough = attribute.value as? Int {
                    if strikethrough == NSUnderlineStyle.single.rawValue {
                        print("*******INIT STRIKETHROUGH******")
                        self.strikethrough = true
                    }
                }
            }
            else if (attribute.key == NSAttributedString.Key.foregroundColor)
            {
                if let foreground = attribute.value as? UIColor {
                    self.foregroundColor = foreground
                }
            }
        }
    }
    
    init(_ attributedString: NSAttributedString, range: NSRange) {
        // RangePointer only tell me how the range of the attributes copied.
//        let rangerPointer = UnsafeMutablePointer<NSRange>.allocate(capacity: 1)
//        rangerPointer.initialize(repeating: NSRange(location: 0, length: 0), count: 1)
        attributes = attributedString.attributes(at: range.location, longestEffectiveRange: nil, in: range)
        
//        print("range \(range) - pointer \(rangerPointer.pointee)")
        
        selectedRange = range
        
        for attribute in attributes {
            if (attribute.key == NSAttributedString.Key.font)
            {
                if let tempFont = attribute.value as? UIFont {
                    uiFont = tempFont
                    fontIndex = tempFont.fontIndex
                    fontSelection = FontSelection(index: tempFont.fontIndex, styleName: "")
                    pointSize = Int(tempFont.pointSize)
                    print("fontName: \(tempFont.fontName)")

                    if tempFont.fontDescriptor.symbolicTraits.contains(.traitBold) {
                        print("*******INIT BOLD******")
                        bold = true
                    }
                    if tempFont.fontDescriptor.symbolicTraits.contains(.traitItalic) {
                        print("*******INIT ITALIC******")
                        italic = true
                    }
                    
                }
            }
            else if (attribute.key == NSAttributedString.Key.underlineStyle)
            {
                if let underlineValue = attribute.value as? Int {
                    if underlineValue == NSUnderlineStyle.single.rawValue {
                        print("*******INIT UNDERLINE******")
                        underline = true
                    }
                }
            }
            else if (attribute.key == NSAttributedString.Key.strikethroughStyle)
            {
                if let strikethrough = attribute.value as? Int {
                    if strikethrough == NSUnderlineStyle.single.rawValue {
                        print("*******INIT STRIKETHROUGH******")
                        self.strikethrough = true
                    }
                }
            }
            else if (attribute.key == NSAttributedString.Key.foregroundColor)
            {
                if let foreground = attribute.value as? UIColor {
                    self.foregroundColor = foreground
                }
            }
        }
    }
    
    init(){
        attributes = [.font: UIFont(name: "Helvetica", size: CGFloat(16))!]
        selectedRange = NSMakeRange(0, 0)
        
        
        for attribute in attributes {
            if (attribute.key == NSAttributedString.Key.font)
            {
                if let tempFont = attribute.value as? UIFont {
                    uiFont = tempFont
                    fontIndex = tempFont.fontIndex
                    fontSelection = FontSelection(index: tempFont.fontIndex, styleName: "")
                    pointSize = Int(tempFont.pointSize)
                    print("fontName: \(tempFont.fontName)")

                    if tempFont.fontDescriptor.symbolicTraits.contains(.traitBold) {
                        print("*******INIT BOLD******")
                        bold = true
                    }
                    if tempFont.fontDescriptor.symbolicTraits.contains(.traitItalic) {
                        print("*******INIT ITALIC******")
                        italic = true
                    }
                    
                }
            }
            else if (attribute.key == NSAttributedString.Key.underlineStyle)
            {
                if let underlineValue = attribute.value as? Int {
                    if underlineValue == NSUnderlineStyle.single.rawValue {
                        print("*******INIT UNDERLINE******")
                        underline = true
                    }
                }
            }
            else if (attribute.key == NSAttributedString.Key.strikethroughStyle)
            {
                if let strikethrough = attribute.value as? Int {
                    if strikethrough == NSUnderlineStyle.single.rawValue {
                        print("*******INIT STRIKETHROUGH******")
                        self.strikethrough = true
                    }
                }
            }
            else if (attribute.key == NSAttributedString.Key.foregroundColor)
            {
                if let foreground = attribute.value as? UIColor {
                    self.foregroundColor = foreground
                }
            }
        }
    }
    
}
