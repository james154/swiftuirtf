//
//  SwiftUIRTFApp.swift
//  Shared
//
//  Created by James Sumners on 9/15/20.
//

import SwiftUI

@main
struct SwiftUIRTFApp: App {
    var body: some Scene {
        // Show Folder and Files View based on UTType defined
        // in the RTFDocument FileDocument struct.
        DocumentGroup(newDocument: RTFDocument()) { file in
            // Open Content View with selected file or created file
            ContentView(document: file.$document)
        }
    }
}
