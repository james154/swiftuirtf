//
//  SwiftUIRTFDocument.swift
//  Shared
//
//  Created by James Sumners on 9/15/20.
//

import SwiftUI
import UniformTypeIdentifiers


struct RTFDocument: FileDocument {
    // Open only rtf file types.
    static var readableContentTypes: [UTType] { [.rtf,.rtfd,.plainText] }
    
    // Define the documents type for the application.
    var text: NSAttributedString
    
    // Initialize a document when creating a new document.
    init(text: NSAttributedString = NSAttributedString(string: "New Note Title") ) {
        self.text = text
    }
    
    // Initialize document from existing file
    init(configuration: ReadConfiguration) throws {
        // Get a local copy of data
        guard let data = configuration.file.regularFileContents
        else {
            throw CocoaError(.fileReadCorruptFile)
        }
        
        // Get a local copy of the contentType
        let contentType = configuration.contentType
        
        // Initialize text
        text = NSAttributedString(string: "")
        
        // Check if content type is rich text with out attachments.
        if contentType == .rtf {
            // Get Attributed String from file data
            // Use do catch to handle throw.
            do {
                let nsAttrString = try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.rtf], documentAttributes: nil)
                text = nsAttrString
            } catch {
//                print("Document is not RTF or RTFD: \(error.localizedDescription)")
            }
            
        }
        // If file plaintext.  This needs to be thought through.
        else if contentType == .plainText {
            let nsAttrString = NSAttributedString(string: String(data: data, encoding: .utf8)!)
            text = nsAttrString
        }
    }
    
    // Wrap NSAttributedString into correct file format
    func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper {
        let saveRange = NSRange(location: 0, length: text.length)
        
        if configuration.contentType == UTType.plainText {
            let data = Data(text.string.utf8)
            
            return .init(regularFileWithContents: data)
        }
        else {
            // Output data from NSAttributedString for RTF
            let data = try text.data(from: saveRange, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.rtf])
            return .init(regularFileWithContents: data)
        }
    }
    
    // Basically the same as fileWrapper.
    func write(to fileWrapper: inout FileWrapper, contentType: UTType) throws {
        let saveRange = NSRange(location: 0, length: text.length)
        
        // Output data from NSAttributedString for RTF
        let data = try text.data(from: saveRange, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.rtf])
        fileWrapper = FileWrapper(regularFileWithContents: data)
    }
}
