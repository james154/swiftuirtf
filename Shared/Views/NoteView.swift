//
//  NoteView.swift
//  SwiftUIRTF
//
//  Created by James Sumners on 8/7/20.
//

import SwiftUI

struct NoteView: View {
    @Binding var text: NSAttributedString
    var body: some View {
        VStack {
            NoteUITextView(attributedString: $text)
                .padding(.horizontal, 5.0)
        }
    }
}

struct NoteUITextView: UIViewRepresentable {
    
    typealias UIViewType = UITextView
    @Binding var attributedString: NSAttributedString
    @State var selectedRange = NSMakeRange(0, 0)
    @ObservedObject var font = FontSelection(index: 2, styleName: "Body")
    
    func makeUIView(context: Context) ->  UITextView {
        let textView = UITextView(frame: CGRect.zero)
        textView.allowsEditingTextAttributes = true
        textView.font = self.font.font
        
        textView.isScrollEnabled = true
        textView.isEditable = true
        textView.isUserInteractionEnabled = true
        textView.adjustsFontForContentSizeCategory = true
        textView.dataDetectorTypes = .link
        
        textView.attributedText = attributedString
        
        textView.setInputAccessory()
        
        textView.delegate = context.coordinator
        
        return textView
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func updateUIView(_ textView: UITextView, context: Context)
    {
        if (textView.attributedText.isEqual(to: attributedString) == false) {
            attributedString = textView.attributedText
        }
    }
    
    // Set Initial Color is ues to change color from black
    // to white when in dark mode.
    func setInitialColor() {
        
        let mutAttrString = NSMutableAttributedString(attributedString: self.attributedString)
        
        let rangerPointer = UnsafeMutablePointer<NSRange>.allocate(capacity: 1)
        rangerPointer.initialize(repeating: NSRange(location: 0, length: 0), count: 1)
        var trackingRange = NSRange(location: 0, length: self.attributedString.length)
        while trackingRange.location < self.attributedString.length {
            let color = (mutAttrString.attribute(.foregroundColor, at: trackingRange.location, longestEffectiveRange: rangerPointer, in: trackingRange) as! UIColor)
            
            if Color(color.cgColor) == .black {
                mutAttrString.addAttribute(.foregroundColor, value: UIColor(.white), range: rangerPointer.pointee)
            }
            
            trackingRange.location += rangerPointer.pointee.length
        }
        self.attributedString = mutAttrString

    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: NoteUITextView
        
        init(_ textView: NoteUITextView) {
            self.parent = textView
        }
        
        func textViewDidChange(_ textView: UITextView) {
            textView.processTextViewChanges()
            
            parent.attributedString = textView.attributedText
        }
        
        func textViewDidChangeSelection(_ textView: UITextView) {
            textView.processSelectionChanges()
            
            
            if textView.inputView?.tag == 1984
            {
                NotificationCenter.default.post(name: NSNotification.Name("TextViewSelectionChange"), object: nil)
            }
            
            parent.attributedString = textView.attributedText
            parent.selectedRange = textView.selectedRange
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            textView.processBeginEdit()
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            textView.processEndEdit()
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            return textView.processReplacement(range: range, text: text);
        }
        
        func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
            //
            return true
        }
        
        func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
            //
            return true
        }
        
        func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
            //
            return true
        }
        
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
            //
            return true
        }
    }
    
}

struct NoteView_Previews: PreviewProvider {
    static var previews: some View {
        NoteView(text: .constant(NSAttributedString(string: "Test")) )
    }
}
