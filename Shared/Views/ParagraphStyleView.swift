//
//  ParagraphStyleView.swift
//  SwiftUIRTF
//
//  Created by James Sumners on 8/30/20.
//

import SwiftUI

struct ParagraphStyleView: View {
    let NC = NotificationCenter.default
    @ObservedObject var paragraphStyle = FontSelection(index: 1, styleName: "Body")
    @ObservedObject var selectFont = FontSelection(index: 1, styleName: "Body")
    @State var textView: UITextView
    @State var allowChange = true
    @State var fontChange = true
    @State var colorChange = true
    
    @State var selectedAttributes: RichTextAttributes
    @State var fontIndex: Int
    @State var foreColor: Color
    @State var pointSize: Int
    @State var bold: Bool
    @State var italic: Bool
    @State var underline: Bool
    @State var strikethrough: Bool
    
//    @State var showMe = false
    var body: some View {
        NavigationView {
            VStack {
                Divider()
                HStack {
                    Spacer()
                    Image(systemName: "multiply.circle")
                        .font(.title)
                        .padding(.vertical, 2)
                        .padding(.trailing, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.black)
                        .onTapGesture(count: 1, perform: {
                            textView.exitInputView()
                        })
                }
                List {
//                    Section(header: Text("Paragraph Style"), content: {
//                        // This will change to a view to select the style rather then the font.
//                        FontSelectionView(fontIndex: $selectedAttributes.fontIndex)
//                    })
                    Section(header: Text("Font"), content: {
                        FontSelectionView(fontIndex: $fontIndex)
                        HStack {
                            Image(systemName: "bold")
                                .foregroundColor(bold ? Color.white : Color.black)
                                .padding(5)
                                .background(RoundedRectangle(cornerRadius: 4)
                                                .stroke(lineWidth: 0.5)
                                                .frame(width: 25.0, height: 25.0)
                                                .background(bold ? Color.black : Color.white))
                                .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                                    
                                    bold.toggle()
                                    
                                    allowChange = false
                                    
                                    selectedAttributes.bold = bold
                                    
                                    textView.updateSelectedAttributes(richAttr: selectedAttributes)
                                })
                            
                            Spacer()
                            
                            Image(systemName: "italic")
                                .foregroundColor(italic ? Color.white : Color.black)
                                .padding(5)
                                .background(RoundedRectangle(cornerRadius: 4)
                                                .stroke(lineWidth: 0.5)
                                                .frame(width: 25.0, height: 25.0)
                                                .background(italic ? Color.black : Color.white))
                                .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                                    italic.toggle()
                                    selectedAttributes.italic = italic
                                    
                                    allowChange = false
                                    textView.updateSelectedAttributes(richAttr: selectedAttributes)
                                })
                            
                            Spacer()
                            
                            Image(systemName: "underline")
                                .foregroundColor(underline ? Color.white : Color.black)
                                .padding(5)
                                .background(RoundedRectangle(cornerRadius: 4)
                                                .stroke(lineWidth: 0.5)
                                                .frame(width: 25.0, height: 25.0)
                                                .background(underline ? Color.black : Color.white))
                                .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                                    underline.toggle()
                                    
                                    selectedAttributes.underline = underline
                                    
                                    allowChange = false
                                    
                                    textView.updateSelectedAttributes(richAttr: selectedAttributes)
                                })
                            
                            Spacer()
                            
                            Image(systemName: "strikethrough")
                                .foregroundColor(strikethrough ? Color.white : Color.black)
                                .padding(5)
                                .background(RoundedRectangle(cornerRadius: 4)
                                                .stroke(lineWidth: 0.5)
                                                .frame(width: 25.0, height: 25.0)
                                                .background(strikethrough ? Color.black : Color.white))
                                .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                                    strikethrough.toggle()
                                    
                                    selectedAttributes.strikethrough = strikethrough
                                    
                                    allowChange = false
                                    textView.updateSelectedAttributes(richAttr: selectedAttributes)
                                })
                        }.padding(.horizontal)
                        HStack {
                            Text("Size")
                            Spacer()
                            Stepper(value: Binding(
                            get: {
                                return pointSize
                            },
                            set: { (newValue) in
                                pointSize = newValue
                                selectedAttributes.pointSize = pointSize
                                
                                allowChange = false
                                textView.updateSelectedAttributes(richAttr: selectedAttributes)
                             }), in: 0...100,
                             label: {
                                Text("\(pointSize) pt")
                             })
                            
                        }
                        ColorPicker("Font Color", selection: $foreColor)
                    })
                }
            }
//            .navigationBarHidden(true)
            .ignoresSafeArea(edges: .bottom)
            
        }
        .onChange(of: foreColor, perform: { value in
            if colorChange {
                selectedAttributes.foregroundColor = UIColor(value)
                
                allowChange = false
                textView.updateSelectedAttributes(richAttr: selectedAttributes)
            }
            colorChange = true
        })
        .onChange(of: fontIndex, perform: { value in
            if fontChange {
                selectedAttributes.fontIndex = value
                
                allowChange = false
                textView.updateSelectedAttributes(richAttr: selectedAttributes)
                
            }
            fontChange = true
        })
        .transition(
            .move(edge: .bottom))
        
        .onAppear{
            let moveAnimation = Animation.easeIn
            
            
            self.NC.addObserver(forName: NSNotification.Name("TextViewSelectionChange"),object: nil, queue: nil) {_ in
                self.refreshView()
            }
            
            return withAnimation(moveAnimation){
//                 print(" animating is hard")
            }
        }
        
    }
    
    
}

extension ParagraphStyleView {
    init(uiTextView: UITextView) {
        _textView = State(initialValue: uiTextView)
        _selectedAttributes = State(initialValue: uiTextView.selectionAttributes)
        _fontIndex = State(initialValue: uiTextView.selectionAttributes.fontIndex)
        _foreColor = State(initialValue: Color(uiTextView.selectionAttributes.foregroundColor.cgColor))
        _pointSize = State(initialValue: uiTextView.selectionAttributes.pointSize)
        _bold = State(initialValue: uiTextView.selectionAttributes.bold)
        _italic = State(initialValue: uiTextView.selectionAttributes.italic)
        _underline = State(initialValue: uiTextView.selectionAttributes.underline)
        _strikethrough = State(initialValue: uiTextView.selectionAttributes.strikethrough)
    }
    
    
    func refreshView() {
        if selectedAttributes.selectedRange != textView.selectionAttributes.selectedRange &&
            allowChange {
            selectedAttributes = textView.selectionAttributes
            
            fontIndex = textView.selectionAttributes.fontIndex
            foreColor = Color(textView.selectionAttributes.foregroundColor.cgColor)
            pointSize = textView.selectionAttributes.pointSize
            bold = textView.selectionAttributes.bold
            italic = textView.selectionAttributes.italic
            underline = textView.selectionAttributes.underline
            strikethrough = textView.selectionAttributes.strikethrough
            fontChange = false
            colorChange = false
        }
        
        allowChange = true
    }
}

struct ParagraphStyleView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
//            NoteView(text: .constant(NSAttributedString(string: "Some text to start working with.\nWill it work?")))
            ParagraphStyleView(uiTextView: UITextView())
        }
    }
}
