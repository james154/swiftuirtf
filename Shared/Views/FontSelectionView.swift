//
//  FontSelectionView.swift
//  SwiftUITest
//
//  Created by James Sumners on 6/25/20.
//  Copyright © 2020 James Sumners. All rights reserved.
//

import SwiftUI
import Combine

struct FontDefault: Codable {
    var index: Int
    var pointSize: Float
    var bold: Bool
    var italic: Bool
    var striketrhough: Bool
    var underline: Bool
    
    func copyCodable() -> UIFont {
        let fontString = UIFont.familyNames.sorted()[index]
        
        var uiFont = UIFont(name: fontString, size: CGFloat(self.pointSize))!
        
        if self.bold {
            uiFont = uiFont.boldToggle()
        }
        
        if self.italic {
            uiFont = uiFont.italicToggle()
        }
        
        return uiFont
    }
}

class FontSelection: ObservableObject {
    var name: String
    @Published var foreground = Color(.black)
    @Published var underline: Bool {
        didSet {
            let tempIndex = index(fromUIFont: self.font)
            let fontDefault = FontDefault(index: tempIndex, pointSize: Float(self.font.pointSize), bold: self.font.isBold, italic: self.font.isItalic, striketrhough: self.strikethrough, underline: self.underline)

            let defaults = UserDefaults.standard
        
            defaults.set(value: fontDefault, forKey: self.name)
        }
    }
    @Published var strikethrough: Bool {
           didSet {
                let tempIndex = index(fromUIFont: self.font)
                let fontDefault = FontDefault(index: tempIndex, pointSize: Float(self.font.pointSize), bold: self.font.isBold, italic: self.font.isItalic, striketrhough: self.strikethrough, underline: self.underline)

                let defaults = UserDefaults.standard
            
                defaults.set(value: fontDefault, forKey: self.name)
           }
       }
    @Published var font: UIFont {
        didSet {
            let tempIndex = index(fromUIFont: self.font)
            if self.fontIndex != tempIndex {
                self.fontIndex = tempIndex
            }
            
            let fontDefault = FontDefault(index: tempIndex, pointSize: Float(self.font.pointSize), bold: self.font.isBold, italic: self.font.isItalic, striketrhough: self.strikethrough, underline: self.underline)

            let defaults = UserDefaults.standard
            
            defaults.set(value: fontDefault, forKey: self.name)
//
//            print(self.font.fontName)
//            print(self.font.familyName)
//            print("isBold:   \(fontDefault.bold)")
//            print("isItalic: \(fontDefault.italic)")
        }
    }
    @Published var fontIndex: Int = 0 {
        didSet {
            let tempFont = font(fromIndex: fontIndex, size: self.font.pointSize)
            
            let oldFamilyName = familyNames[oldValue]
            
            if oldFamilyName != tempFont.familyName {
                font = tempFont
            }
        }
    }
    
    public let familyNames = UIFont.familyNames.sorted()
    
    init(font uiFont: UIFont, styleName: String) {
        
        self.name = styleName.replacingOccurrences(of: "-", with: "")
        self.underline = false
        self.strikethrough = false
        
        if let fontDefault: FontDefault? = UserDefaults.standard.codable(forKey: styleName) {
            self.font = fontDefault!.copyCodable()
            if fontDefault!.bold {
                self.font = self.font.withTraits(.traitBold)
            }
//            self.fontIndex = self.index(fromUIFont: self.font)
            self.underline = fontDefault!.underline
            self.strikethrough = fontDefault!.striketrhough
        }
        else {
            self.font = uiFont
            self.fontIndex = self.index(fromUIFont: uiFont)
        }
    }
    
    init(index: Int, styleName: String){
        
        self.name = styleName.replacingOccurrences(of: "-", with: "")
        self.underline = false
        self.strikethrough = false
        
        if let fontDefault: FontDefault? = UserDefaults.standard.codable(forKey: styleName) {
            self.font = fontDefault!.copyCodable()
            if fontDefault!.bold {
                self.font = self.font.withTraits(.traitBold)
            }
//            self.fontIndex = self.index(fromUIFont: self.font)
            self.underline = fontDefault!.underline
            self.strikethrough = fontDefault!.striketrhough
        }
        else {
            let familyNames = UIFont.familyNames.sorted()
            self.font = UIFont(name: familyNames[index], size: CGFloat(16))!
            self.fontIndex = index
        }
    }
    
    init(_ selection: RichTextAttributes) {
        self.font = selection.uiFont
        self.underline = selection.underline
        self.strikethrough = selection.strikethrough
        self.name = "_"
        
    }
    
    func font(fromIndex: Int, size: CGFloat?) -> UIFont {
        var tempIndex: Int = fromIndex
        
        if fromIndex > self.familyNames.count ||
            fromIndex < 0{
            tempIndex = 0
        }
        
        if size != nil {
            return UIFont(name: self.familyNames[tempIndex], size: size!)!
        }
        else {
            return UIFont(name: self.familyNames[tempIndex], size: self.font.pointSize)!
        }
    }
    
    func index(fromUIFont: UIFont) -> Int {
        return self.familyNames.firstIndex(of: fromUIFont.familyName)!
    }
    
    func families() -> [String] {
        return self.familyNames
    }
    
    func toggle(forTrait: UIFontDescriptor.SymbolicTraits) {
        let fontDesc = self.font.fontDescriptor
        
        if fontDesc.symbolicTraits.contains(forTrait) {
//            print("OFF: \(forTrait)")
            var traits = fontDesc.symbolicTraits
            traits.remove(forTrait)
            
            self.font = UIFont(descriptor: fontDesc.withSymbolicTraits(traits)!, size: 0)
        }
        else if forTrait == .traitBold {
//            print("ON : \(forTrait)")
            self.font = self.font.boldToggle()
//            print(self.font)
        }
        else if forTrait == .traitItalic {
//            print("ON : \(forTrait)")
            self.font = self.font.italicToggle()
//            print(self.font)
        }
        
    }
    
    
    
}

struct FontSelectionView: View {
    @State var title = "Select Font"
    @Binding var fontIndex: Int
    
    let fontSelection = RichTextAttributes()
    
    var body: some View {
        Picker(selection: $fontIndex, label: Text(title)){
            ForEach(0..<fontSelection.familyNames.count) { family in
                Text(self.fontSelection.familyNames[family])
                    .font(UIFont(name: self.fontSelection.familyNames[family], size: CGFloat(16))?.font)
            }
            .navigationBarHidden(true)
        }
    }
}

struct FontChangeView: View {
    @State var name:String
    @Binding var fontSelect: RichTextAttributes
    var body: some View {
        NavigationView {
            VStack {
                List {
                    Text(fontSelect.uiFont.familyName)
                        .font(fontSelect.uiFont.font)
                        .underline(fontSelect.underline)
                        .strikethrough(fontSelect.strikethrough)
                    FontSelectionView(fontIndex: $fontSelect.fontIndex)
                    
                    Stepper(value: Binding(
                                get: {
                                    return fontSelect.pointSize
                                },
                                set: { (newValue) in
                                    
                                    fontSelect.pointSize = newValue
                                    
//                                    print("pointSize: \(fontSelect.pointSize)")
                                }), in: 0...100,
                            label: {
                                Text("\(fontSelect.pointSize) pt")
                            })
                    
                    HStack {
                        Image(systemName: "bold")
                            .padding(5)
                            .background(RoundedRectangle(cornerRadius: 4)
                                            .stroke(lineWidth: 0.5)
                                            .frame(width: 25.0, height: 25.0))
                            .onTapGesture(count: 1, perform: {
                                fontSelect.bold.toggle()
                            })
                        
                        Spacer()
                        
                        Image(systemName: "italic")
                            .padding(5)
                            .background(RoundedRectangle(cornerRadius: 4)
                                            .stroke(lineWidth: 0.5)
                                            .frame(width: 25.0, height: 25.0))
                            .onTapGesture(count: 1, perform: {
                                fontSelect.italic.toggle()
                            })
                        
                        Spacer()
                        
                        Image(systemName: "underline")
                            .padding(5)
                            .background(RoundedRectangle(cornerRadius: 4)
                                            .stroke(lineWidth: 0.5)
                                            .frame(width: 25.0, height: 25.0))
                            .onTapGesture(count: 1, perform: {
                                fontSelect.underline.toggle()
                            })
                        
                        Spacer()
                        
                        Image(systemName: "strikethrough")
                            .padding(5)
                            .background(RoundedRectangle(cornerRadius: 4)
                                            .stroke(lineWidth: 0.5)
                                            .frame(width: 25.0, height: 25.0))
                            .onTapGesture(count: 1, perform: {
                                fontSelect.strikethrough.toggle()
                            })
                    }.padding(.horizontal)
                }
                
                
                
                
            }.navigationBarTitle(Text(self.name))
            .onChange(of: "fontSelect") { value in
//                print("Selected Font: \(fontSelect.uiFont.fontName)")
//                print("Selected Font: \(fontSelect.pointSize)")
//                print("Selected Font: \(fontSelect.bold)")
            }
        }
        
    }
}

struct FontSummaryView: View {
    @State var name: String
    @ObservedObject var fontSelect: RichTextAttributes
    var body: some View {
        HStack {
            Text(name)
                .font(fontSelect.uiFont.font)
                .underline(fontSelect.underline)
                .frame(width: 80.0)
            Divider()
            Text(fontSelect.uiFont.familyName).font(fontSelect.uiFont .font)
            Spacer()
            Text("\(Int(fontSelect.pointSize))").font(fontSelect.uiFont.font).multilineTextAlignment(.leading)
        }
    }
}

struct FontSelectionView_Previews: PreviewProvider {

    static var previews: some View {
        FontChangeView(name: "Header", fontSelect: .constant(RichTextAttributes()))
    }
}
