//
//  ContentView.swift
//  Shared
//
//  Created by James Sumners on 9/15/20.
//

import SwiftUI

struct ContentView: View {
    @Binding var document: RTFDocument

    var body: some View {
        // NoteView is a UITextView UIViewRepresentable for SwiftUI
        NoteView(text: $document.text)
            
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(document: .constant(RTFDocument()))
    }
}
