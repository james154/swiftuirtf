//
//  FontSettings.swift
//  FontProject
//
//  Created by James Sumners on 7/12/20.
//

import SwiftUI
import Combine

struct FontSettings: View {
    @StateObject private var titleStyle = FontSelection(index: 0, styleName: "title")
    @StateObject var subTitleStyle = FontSelection(index: 1, styleName: "subtitle")
    @StateObject var bodyStyle = FontSelection(index: 2, styleName: "body")
    @StateObject var bulletStyle = FontSelection(index: 3, styleName: "bullets")
    var body: some View {
        NavigationView {
            
            Form {
                NavigationLink(destination:
                               FontChangeView(name: "Title", fontSelect: titleStyle), label: {
                    FontSummaryView(name: "Title", fontSelect: titleStyle)
                })
                NavigationLink(destination: FontChangeView(name: "Sub-title", fontSelect: self.subTitleStyle), label: {
                    FontSummaryView(name: "Sub-Title", fontSelect: self.subTitleStyle)
                })
                NavigationLink(destination: FontChangeView(name: "Body", fontSelect: self.bodyStyle), label: {
                    FontSummaryView(name: "Body", fontSelect: self.bodyStyle)
                })
                NavigationLink(destination: FontChangeView(name: "Bullets", fontSelect: self.bulletStyle), label: {
                    FontSummaryView(name: "Bullets", fontSelect: self.bulletStyle)
                })
            }.navigationBarTitle(Text("FONT SETTINGS"))
        }
    }
}

struct FontSettings_Previews: PreviewProvider {
    static var previews: some View {
        FontSettings()
    }
}
